package at.simma.Maturaaufgabe1;

import java.lang.reflect.Array;
import java.util.*;
import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Maturaaufgabe1 extends BasicGame {
    private ArrayList<Regler> regler;

    public Maturaaufgabe1() {
        super("Landscape");
    }

    @Override
    public void init(GameContainer arg0) throws SlickException {
        regler = new ArrayList<>();
        regler.add(new Heizung());
        regler.add(new Markise());
    }

    @Override
    public void render(GameContainer gc, Graphics graphics) throws SlickException {
        for (Regler item : regler) {
            item.render(graphics);
        }
    }

    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        for (Regler item : regler) {
            item.update(gc, delta);
        }
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new Maturaaufgabe1());
            container.setDisplayMode(600, 600, false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
