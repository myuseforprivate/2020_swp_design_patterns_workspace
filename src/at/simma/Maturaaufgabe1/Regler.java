package at.simma.Maturaaufgabe1;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Regler {
    void update(GameContainer gc, int delta);
    void render(Graphics graphics);
}
