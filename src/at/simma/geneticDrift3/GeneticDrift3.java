package at.simma.geneticDrift3;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class GeneticDrift3 {
    public String input = "";

    public static int[] permutation = {125,133,134,135,136,-52,-51,-50,-49,-48,-47,-46,-45,66,67,68,69,70,71,-38,-37,-36,-35,-34,-33,-32,-31,-30,-29,-132,-131,-130,-193,-192,-191,-190,-189,-188,-187,-186,-185,-184,-183,-182,-181,-180,-179,-178,-177,-176,-175,-174,-173,-172,-171,-170,-169,-77,-76,-75,-74,-73,-72,18,19,20,21,22,23,24,25,26,27,28,-164,-163,-65,-64,-63,-62,-61,-60,-59,-58,-57,-56,-55,-54,-53,39,40,41,42,43,44,159,160,161,162,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,-168,-167,-166,-165,126,127,128,129,86,87,88,89,90,91,92,93,94,95,96,-124,-123,-122,-121,-120,-119,-118,-117,-116,-115,-114,-113,-112,-111,-110,-109,-108,-107,-106,-105,-104,-103,-102,-101,-100,-99,-98,-97,153,154,155,156,157,158,-148,-147,-146,-145,-144,-143,-142,-141,-140,-139,-138,-137,-85,-84,-83,-82,-81,-80,-79,-78,-152,-151,-150,-149};
    public int[] pair = {-45, 44};

    public static void main(String[] argv) {
        System.out.println(getAmountOfPairsAfterInversion(-45, 44));
        ArrayList<int[]> res = getOrientatedPairs();

        for (int[] item : res) {
            System.out.println(item[0] + " " + item[1]);
        }



    }

    public static int getAmountOfPairsAfterInversion(int valueA, int valueE) {
        int indexA = 0, indexE = 0, result = 0;
        int[] inversion = new int[0];

        // find indexes of the given pair values
        for (int i = 0; i < permutation.length; i++) {
            if (permutation[i] == valueA) {
                indexA = i;
            } else if (permutation[i] == valueE) {
                indexE = i;
            }
        }
        System.out.println("Indexes found: " + indexA + " " + indexE);
        System.out.println();

        // get the values to inverse
        if (permutation[indexA] + permutation[indexE] == 1) {
            inversion = Arrays.copyOfRange(permutation, indexA, indexE);
        } else if (permutation[indexA] + permutation[indexE] == -1) {
            inversion = Arrays.copyOfRange(permutation, indexA + 1, indexE + 1);
            indexA+=1;
        }

        // perform inversion
        for (int j = 0; j < inversion.length; j++) {
            inversion[j] = inversion[j] * -1;
        }

        for (int ii = 0; ii < inversion.length / 2; ii++) {
            int temp = inversion[ii];
            inversion[ii] = inversion[inversion.length - 1 - ii];
            inversion[inversion.length - 1 - ii] = temp;
        }

        System.out.print("Inversed Array: ");
        for (int z = 0; z < inversion.length; z++) {
            System.out.print(inversion[z] + " ");
        }
        System.out.println();

        //write back to permutation
        for (int k = 0; k < inversion.length; k++) {
            permutation[indexA+k] = inversion[k];
        }

        // calculate pairs of the inversed array
        for (int i = 0; i < permutation.length; i++) {
            for (int j = i; j < permutation.length; j++) {
                if (i != j) {
                    if (permutation[i] >= 0 && permutation[j] < 0 || permutation[i] < 0 && permutation[j] >= 0) {
                        if (Math.abs(Math.abs(permutation[i]) - Math.abs(permutation[j])) == 1) {
                            result ++;
                        }
                    }
                }
            }
        }

        return result;
    }



    public static ArrayList getOrientatedPairs() {
        ArrayList<int[]> result = new ArrayList<>();

        for (int i = 0; i < permutation.length; i++) {
            for (int j = i; j < permutation.length; j++) {
                if (i != j) {
                    if (permutation[i] >= 0 && permutation[j] < 0 || permutation[i] < 0 && permutation[j] >= 0) {
                        if (Math.abs(Math.abs(permutation[i]) - Math.abs(permutation[j])) == 1) {
                            result.add(new int[] {permutation[i], permutation[j]});
                        }
                    }
                }
            }
        }
        return result;
    }
}

