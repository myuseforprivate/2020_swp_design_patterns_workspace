package at.simma.Maturaaufgabe2;

public class Car {
    private InjectionStrategy injectionStrategy;

    public Car(InjectionStrategy injectionStrategy) {
        this.injectionStrategy = injectionStrategy;
    }

    public InjectionStrategy getInjectionStrategy() {
        return injectionStrategy;
    }

    public void drive(){
        this.injectionStrategy.drive();
    }
}
