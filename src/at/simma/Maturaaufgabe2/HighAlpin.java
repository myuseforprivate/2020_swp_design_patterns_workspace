package at.simma.Maturaaufgabe2;

public class HighAlpin implements InjectionStrategy{
    private int motion;

    public HighAlpin(int motion) {
        this.motion = motion;
    }

    @Override
    public int drive() {
        System.out.println("I am driving alpine. speed: " + this.motion);
        return this.motion;
    }


}
