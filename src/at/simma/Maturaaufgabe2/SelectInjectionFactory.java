package at.simma.Maturaaufgabe2;

public class SelectInjectionFactory {
    public static InjectionStrategy getStrategy(String location) {
        if (location == "Marokko") {
            return new African(1);
        } else if (location == "Österreich"){
            return new HighAlpin(3);
        }
        return new HighAlpin(4);
    }
}
