package at.simma.Maturaaufgabe2;

public class African implements InjectionStrategy {
    private int motion;

    public African(int motion) {
        this.motion = motion;
    }

    @Override
    public int drive() {
        System.out.println("I am driving. speed:" + this.motion);
        return this.motion;
    }


}
