package at.simma.Maturaaufgabe2;

public class Main {
    public static void main(String[] argv) {
        InjectionStrategy s1 = SelectInjectionFactory.getStrategy("Marokko");

        Car c1 = new Car(s1);
        Car c2 = new Car(s1);

        c1.drive();
        c2.drive();

    }
}
