package at.simma.landscape;

public interface Observer {
    public void inform();
}
