package at.simma.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import java.awt.*;

public class HTLRect extends AbstractActor implements Observer {
	private double width, height, x, y, degree, speed, radius, centerX, centerY;
	private org.newdawn.slick.Color color;

	public HTLRect(MoveStrategy moveStrategy, double centerY, double width, double height, double speed, double degree, double radius) {
		super(moveStrategy);
		this.width = width;
		this.height = height;
		this.degree = degree;
		this.speed = speed;
		this.radius = radius;
		this.color = org.newdawn.slick.Color.blue;
	}

	public void render(Graphics graphics) {
		graphics.setColor(this.color);
		graphics.fillRect((float)this.moveStrategy.getX(), (float)this.moveStrategy.getY(), (float)this.width, (float)this.height);
		graphics.setColor(Color.white);
	}

	public void inform() {
		System.out.println("HAllo");
		this.color = org.newdawn.slick.Color.red;
	}
	
	
	
}
