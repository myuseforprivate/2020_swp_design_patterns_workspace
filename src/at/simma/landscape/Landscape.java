package at.simma.landscape;

import java.util.ArrayList;
import java.util.List;

import at.simma.singleton.CounterSingleton;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Landscape extends BasicGame {
	private ArrayList<Actor> actors;
	private Player player;
	private ArrayList<Actor> actorsToAdd;
	private CounterSingleton cs, cs2;
	
	public Landscape() {
		super("Landscape");
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		this.actors = new ArrayList<>();
		this.actorsToAdd = new ArrayList<>();

		/*
		for (int i = 0; i < 50; i++) {
			this.actors.add(new Snowflake("small"));
			this.actors.add(new Snowflake("middle"));
			this.actors.add(new Snowflake("large"));
		}
		*/
		player = new Player(10, 10, 20, 20, 0.01, this);
		actors.add(player);
		actors.add(new HTLCircle(new MoveRight(1,1,0.05f),10,10));
		actors.add(new HTLRect(new MoveLeft(500,100,0.01f),20,10, 10, 10, 10, 10));

		HTLRect htlRect = new HTLRect(new MoveLeft(500,100,0.01f),20,10, 10, 10, 10, 10);
		HTLRect htlRect2 = new HTLRect(new MoveLeft(400,100,0.01f),20,10, 10, 10, 10, 10);
		HTLCircle htlCircle = new HTLCircle(new MoveRight(1,1,0.05f),10,10);

		this.player.addObserver(htlRect);
		this.player.addObserver(htlRect2);
		this.player.addObserver(htlCircle);
		actors.add(htlRect);
		actors.add(htlRect2);
		actors.add(htlCircle);

		for (int i = 0; i < 10; i++) {
			this.actors.add(RandomCircleFactory.getRandomActor());
		}

		cs = CounterSingleton.getInstance();
		cs2 = CounterSingleton.getInstance();

		this.cs.increaseCounter();
		this.cs2.increaseCounter();
		System.out.println(cs.getCounter() + " " + cs2.getCounter());

		//actors.add(new HTLRect(100,100,10,10,0.01,0.1,100));
		//actors.add(new HTLOval(10,10,10,10,0.1));
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		for (Actor actor : actors) {
			actor.render(graphics);
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actor actor : actors) {
			actor.update(gc, delta);
		}
		
		if (this.actorsToAdd.size()>0) {
			for (Actor a : this.actorsToAdd) {
				this.actors.add(a);
			}
			this.actorsToAdd.clear();
		}

	}

	void addActor(Actor actor) {
		this.actorsToAdd.add(actor);
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(600, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
