package at.simma.landscape;

public interface MoveStrategy {
    public float getX();
    public float getY();
    public void update(int delta);
}
