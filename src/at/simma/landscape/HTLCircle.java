package at.simma.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLCircle extends AbstractActor implements Observer {
	private double diameter, speed;
	private org.newdawn.slick.Color color;

	public HTLCircle(MoveStrategy moveStrategy, double diameter, double speed) {
		super(moveStrategy);
		this.diameter = diameter;
		this.speed = speed;
		this.color = Color.green;
	}

	@Override
	public void render(Graphics graphics) {
		graphics.setColor(this.color);
		graphics.fillOval((float)this.moveStrategy.getX(), (float)this.moveStrategy.getY(), (float)this.diameter, (float)this.diameter);
		graphics.setColor(Color.white);
	}

	public void inform() {
		this.color = Color.darkGray;
	}
}
