package at.simma.landscape;

import java.util.Random;

public class RandomCircleFactory {
    public static Actor getRandomActor() {
        Random random = new Random();
        int number = random.nextInt(2);

        if (number == 0) {
            return new HTLCircle(new MoveRight(1,1,0.05f),random.nextInt(30),10);
        }
        else {
            return new HTLRect(new MoveLeft(500,100,0.01f),random.nextInt(50),random.nextInt(50), random.nextInt(50), random.nextInt(50), random.nextInt(50), 10);
        }

    }
}
