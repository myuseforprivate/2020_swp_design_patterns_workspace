package at.simma.geneticDrift;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GeneticDrift {
    static String inputString = "6 3 1 6 5 -2 4 1 -2";
    static int inputLength = 0;
    static String[] inputStringArray;
    static int[] inputIntArray;
    static ArrayList<int[]> result;


    public static void main(String[] argv) {
        // init
        int colonPos = inputString.indexOf(" ");

        int inputLength = Integer.valueOf(inputString.substring(0, colonPos));

        inputStringArray = inputString.substring(colonPos+1, inputString.length()).split(" ");

        inputIntArray = new int[inputStringArray.length];

        result = new ArrayList<>();

        // parse string array to int array
        for ( int i = 0; i < inputStringArray.length; i++) {
            inputIntArray[i] = Integer.valueOf(inputStringArray[i]);
        }



        for (int i = 0; i < inputIntArray.length; i++) {
            for (int j = i; j < inputIntArray.length; j++) {
                if (i != j) {
                    if (inputIntArray[i] >= 0 && inputIntArray[j] < 0 || inputIntArray[i] < 0 && inputIntArray[j] >= 0) {
                        if (Math.abs(Math.abs(inputIntArray[i]) - Math.abs(inputIntArray[j])) == 1) {
                            result.add(new int[] {inputIntArray[i], inputIntArray[j]});
                        }
                    }
                }
            }
        }

        Collections.sort(result, new Comparator<int[]>() {
            public int compare(int[] a, int[] b) {
                return a[0] -b[0];
            }
        });

        // print out 0
        System.out.print(result.size());

        for ( int[] r : result) {
            System.out.print(" " + r[0] + " " + r[1]);
        }

        /*
        outputArray = new int[numOfThrows];

        String[] tmpThrows = inputString.substring(colonPos+1, inputString.length()).split(",");

        inputArray = new int[tmpThrows.length];

        for(int i = 0;i < tmpThrows.length;i++)
        {
            // Note that this is assuming valid input
            // If you want to check then add a try/catch
            // and another index for the numbers if to continue adding the others (see below)
            inputArray[i] = Integer.parseInt(tmpThrows[i]);
        }


        //calculate
        int tmpv = 0;
        int sub = 0;
        for (int i = 0; i < numOfThrows*2-sub; i+=2) {
            if (inputArray[i] == 10) {
                int tmpScore = inputArray[i] + inputArray[i + 1] + inputArray[i + 2];

                tmpv+=tmpScore;
                i -= 1;
                sub++;
            } else {
                int tmpScore = inputArray[i] + inputArray[i + 1];

                if (tmpScore == 10) {
                    tmpScore += inputArray[i + 2];

                }

                tmpv+=tmpScore;
            }
            System.out.print(tmpv + ",");
}
         */

    }
}
