package at.simma.geneticDrift2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class GeneticDrift2 {
    static String[] input = "6 3 1 6 5 -2 4 1 -2".split(" ");
    static int[] inputInt = new int[input.length], permutation;
    static ArrayList<int[]> pairs;

    public static void main(String[] argv) {
        for (int i = 0; i < input.length; i++) {
            inputInt[i] = Integer.valueOf(input[i]);
        }

        for (int i = 1; i < inputInt.length; i++) {
        //    System.out.print(inputInt[i]);
        }

        permutation = Arrays.copyOfRange(inputInt, 1, inputInt[0]+1);

        for (int i = 0; i < permutation.length; i++) {
        //    System.out.print(permutation[i]);
        }

        pairs = new ArrayList<>();

        for (int i = inputInt[0]+1; i < inputInt.length; i+=4) {
            pairs.add(new int[] {inputInt[i], inputInt[i+1], inputInt[i+2], inputInt[i+3]});
        }

        for (int i = 0; i < pairs.size(); i++) {
int a = 0;
            int[] inversion;


            if (pairs.get(i)[0] + pairs.get(i)[2] == 1) {
                inversion = Arrays.copyOfRange(permutation, pairs.get(i)[1], pairs.get(i)[3]);
                a = pairs.get(i)[1];
            } else if (pairs.get(i)[0] + pairs.get(i)[2] == -1) {
                inversion = Arrays.copyOfRange(permutation, pairs.get(i)[1]+1, pairs.get(i)[3]+1);
                a = pairs.get(i)[1]+1;
            } else {
                inversion = new int[0];
            }


            for (int j = 0; j < inversion.length; j++) {
                inversion[j] = inversion[j] * -1;
            }


            for (int ii = 0; ii < inversion.length / 2; ii++) {
                int temp = inversion[ii];
                inversion[ii] = inversion[inversion.length - 1 - ii];
                inversion[inversion.length - 1 - ii] = temp;
            }

            for (int k = 0; k < inversion.length; k++) {
                System.out.print(inversion[k] + " ");
                permutation[a+k] = inversion[k];
            }




            for (int k = 0; k < permutation.length; k++) {
            //    System.out.print(permutation[k] + " ");
            }


        }


    }
}
