package at.simma.bowling;

public class BowlingV1 {
    static String inputString = "10:7,2,1,9,6,4,5,5,10,3,7,7,3,6,4,10,2,8,6";
    static String output = "";
    static int numOfThrows = 0;
    static int[] inputArray, outputArray;
    static int colonPos = 0;

    public static void main(String[] argv) {
        // init
        colonPos = inputString.indexOf(":");

        int numOfThrows = Integer.valueOf(inputString.substring(0, colonPos));

        outputArray = new int[numOfThrows];

        String[] tmpThrows = inputString.substring(colonPos+1, inputString.length()).split(",");

        inputArray = new int[tmpThrows.length];

        for(int i = 0;i < tmpThrows.length;i++)
        {
            // Note that this is assuming valid input
            // If you want to check then add a try/catch
            // and another index for the numbers if to continue adding the others (see below)
            inputArray[i] = Integer.parseInt(tmpThrows[i]);
        }


        //calculate
        int tmpv = 0;
        int sub = 0;
        for (int i = 0; i < numOfThrows*2-sub; i+=2) {
            if (inputArray[i] == 10) {
                int tmpScore = inputArray[i] + inputArray[i + 1] + inputArray[i + 2];

                tmpv+=tmpScore;
                i -= 1;
                sub++;
            } else {
                int tmpScore = inputArray[i] + inputArray[i + 1];

                if (tmpScore == 10) {
                    tmpScore += inputArray[i + 2];

                }

                tmpv+=tmpScore;
            }
            System.out.print(tmpv + ",");
        }
    }
}
