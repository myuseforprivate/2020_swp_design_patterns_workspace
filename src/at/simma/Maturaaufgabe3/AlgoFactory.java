package at.simma.Maturaaufgabe3;

public class AlgoFactory {
    public static Algo getAlgo(String os) throws Exception {
        if (os == "Windows") {
            return new BillGatesAlgo();
        } else if (os == "Apple"){
            return new SteveJobbsAlgo();
        } else if (os == "Linux") {
            return new LinusTorvaldAlgo();
        } else{
        throw new Exception("Dieser Algorithmus existiert nicht.");
        }
    }
}
