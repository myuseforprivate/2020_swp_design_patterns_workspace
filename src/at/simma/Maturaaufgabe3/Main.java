package at.simma.Maturaaufgabe3;

public class Main {
    public static void main(String[] argv) {
        Algo s1 = null;
        try {
            s1 = AlgoFactory.getAlgo("Windows");
            System.out.println(s1.getOutput("Hallo"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Algo s2 = null;
        try {
            s2 = AlgoFactory.getAlgo("Raspian");
            System.out.println(s2.getOutput("Hallo"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
