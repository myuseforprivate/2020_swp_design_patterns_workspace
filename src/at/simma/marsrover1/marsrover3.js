function main() {
    //let input = "1.00 3 1.00 15.00 1.00 0.00 1.00 -15.00".split(" ");
    let input = "1.09 4 9.86 10 9.86 10 9.86 10 9.86 10".split(" ");

    let wheelBase = parseFloat(input[0]), numOfSegments = parseFloat(input[1]);
    let segments = [];

    for (let i = 2; i < input.length; i += 2) {
        segments[i / 2 - 1] = [parseFloat(input[i]), parseFloat(input[i + 1])];
    }

    let tmp = { newX: 0, newY: 0, newDirection: 0 };

    for (let i = 0; i < segments.length; i++) {
        tmp = getPosition(wheelBase, segments[i][0], segments[i][1], tmp.newX, tmp.newY, tmp.newDirection);



        console.log(round(tmp.newX) + " " + round(tmp.newY) + " " + round(tmp.newDirection));
    }


}

main();

function round(number) {
    return Math.round(number * 100) / 100;
}

function getPosition(wheelBase, distance, steeringAngle, oldX, oldY, oldDirection) {
    let turnRadius = wheelBase / Math.sin(steeringAngle * Math.PI / 180);

    let newX, newY, newDirection;

    if (steeringAngle == 0) {
        newX = 0.00;
        newY = distance;
        newDirection = 0.00;
    } else {
        newDirection = 180 * distance / (Math.PI * turnRadius);

        newY = Math.sin(newDirection * Math.PI / 180) * turnRadius;
        newX = turnRadius - (Math.cos(newDirection * Math.PI / 180)) * turnRadius;

        while (newDirection < 0) {
            newDirection += 360;
        }
    }

    newX += oldX
    newY += oldY


    return { newX, newY, newDirection }
}