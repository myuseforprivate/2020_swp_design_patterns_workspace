package at.simma.marsrover1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Marsrover1 {
    public static void main(String[] argv) {

        String[] input = "2.45 90.00".split(" ");

        double wheelBase = Double.parseDouble(input[0]);
        double steeringAngle = Double.parseDouble(input[1]);

        System.out.println(wheelBase+ " " + steeringAngle);

        double result = wheelBase/Math.sin((steeringAngle * Math.PI / 180));

        System.out.println(result);
    }
}
