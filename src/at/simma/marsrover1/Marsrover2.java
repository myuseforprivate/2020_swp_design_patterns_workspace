package at.simma.marsrover1;

public class Marsrover2 {
    public static void main(String[] argv) {

        String[] input = "1.00 1.00 30.00".split(" ");

        double wheelBase = Double.parseDouble(input[0]);
        double distance = Double.parseDouble(input[1]);
        double steeringAngle = Double.parseDouble(input[2]);

        System.out.println(wheelBase+ " " + steeringAngle);

        double turnRadius = wheelBase/Math.sin(steeringAngle * Math.PI / 180);
        System.out.println(turnRadius);

        double newY = Math.sin(steeringAngle*Math.PI/180)*turnRadius;

        System.out.println("asdf" + newY);
    }
}
